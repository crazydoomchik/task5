﻿using CalculatorLibrary;
using System;
using System.IO;

namespace ConsoleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            string expression;

            Parser parser = new Parser();
            

            while (true)
            {
                Console.WriteLine("Enter 1 for user input \nEnter 2 for parse file");

                string userChoice = Console.ReadLine();

                if (userChoice.Equals("1"))
                {
                    while (true)
                    {
                        try
                        {
                            Console.WriteLine("Enter expression");
                            expression = Console.ReadLine();

                            Console.WriteLine(parser.Evaluate(expression));
                        }
                        catch(ParserException exe)
                        {
                            Console.WriteLine(exe);
                        }
                    }
                }
                else if(userChoice.Equals("2"))
                {
                    string path;

                    Console.WriteLine("Enter path to file");
                    path = Console.ReadLine();

                    if (!File.Exists(path))
                    {
                        Console.WriteLine($"Error: Enter correct path!");
                        continue;
                    }

                    ParseSaveFile parseSave = new ParseSaveFile();

                    parseSave.ParseFile(path);

                    Console.WriteLine("Enter path to Save File");
                    path = Console.ReadLine();

                    try
                    {
                        parseSave.SaveFile(path);
                        Console.WriteLine("File save succes");
                    }
                    catch(FormatException exe)
                    {
                        Console.WriteLine(exe.Message);
                    }

                    continue;
                }            
                Console.WriteLine("Incorrect input");
            }

        }
    }
}
