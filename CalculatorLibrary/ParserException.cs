﻿using System;


namespace CalculatorLibrary
{
    public class ParserException : ApplicationException
    {

        public ParserException(string str) : base(str)
        {
               
        }
        public ParserException(Errors errors): this(SyntaxErr(errors))
        {

        }

        public override string ToString()
        {
            return Message;
        }

        public static string SyntaxErr(Errors error)
        {
            string[] err =
            {
                "Syntax error",
                "Imbalance of brackets",
                "Null",
                "Division by zero"
            };
            
            return (err[(int)error]);
        }


    }
}
