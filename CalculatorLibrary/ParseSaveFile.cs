﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CalculatorLibrary
{
    public class ParseSaveFile
    {
        public List<string> Expression { get; set; } = new List<string>();

        
        public void ParseFile(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Unable to find the specified file.");
            }

            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {

                string line;
                Parser parser = new Parser();

                while ((line = sr.ReadLine()) != null)
                {
                    try
                    {
                        line = AddToLine(line, parser.Evaluate(line).ToString());
                        Expression.Add(line);
                    }
                    catch(ParserException exe)
                    {
                        line = AddToLine(line, exe.Message);
                        Expression.Add(line);
                    }
                }
            }
        }

        public void SaveFile(string path)
        {
            string writePath = path;

            if(String.IsNullOrEmpty(path))
            {
                throw new FormatException("Wrong path");
            }
            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {
                    foreach (var item in Expression)
                    {
                        sw.WriteLine(item);
                    }
                }
            }
            catch(Exception)
            {
                
            }
        }

        private string AddToLine(string line, string join)
        {
            return line += $" = {join}";
        }
    }
}
