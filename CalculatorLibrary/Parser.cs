﻿using System;
using System.Collections.Generic;

namespace CalculatorLibrary
{
    public class Parser
    {

        private string _expression;
        private int _expIndex;
        private string _token;

        public double Evaluate(string expStr)
        {
            _expression = expStr;
            _expIndex = 0;

            ChangeToken();

            if(String.IsNullOrEmpty(_token))
                {
                throw new ParserException(Errors.NoExpressions);
                }

            double result = ActPlusOrMinus();

            if (!String.IsNullOrEmpty(_token)) throw new ParserException(Errors.Syntax);

            return result;

        }

        private double ActPlusOrMinus()
        {

            double firstOperand = ActMultOrDiv();

            Operator @operator;

            while ((@operator = GetOperator())  == Operator.Plus || @operator == Operator.Minus)
            {
                ChangeToken();

                double secondOperand = ActMultOrDiv();

                switch (@operator)
                {
                    case Operator.Plus:
                        firstOperand = firstOperand + secondOperand;
                        break;
                    case Operator.Minus:
                        firstOperand = firstOperand - secondOperand;
                        break;
                }
                
            }
            return firstOperand;
        }

        private double ActMultOrDiv()
        {

            double firstOperand = ActExponentiation();
            Operator @operator;

            while ((@operator = GetOperator()) == Operator.Multiply || @operator == Operator.Division || @operator == Operator.RemaiderDivision)
            {
                ChangeToken();
                double secondOperand = ActExponentiation();

                switch (@operator)
                {
                    case Operator.Multiply:
                        firstOperand = firstOperand * secondOperand;
                        break;
                    case Operator.Division:
                        if (secondOperand == 0.0) throw new ParserException(Errors.DivByZero);

                        firstOperand = firstOperand / secondOperand;
                        break;
                    case Operator.RemaiderDivision:
                        if (secondOperand == 0.0) throw new ParserException(Errors.DivByZero);

                        firstOperand = (int)firstOperand % (int)secondOperand;
                        break;
                }

            }
            return firstOperand;
        }

        private double ActExponentiation()
        {

            double firstOperand = ActUnaryOperator();

            Operator @operator;

            while ((@operator = GetOperator()) == Operator.Exponent)
            {
                ChangeToken();

                double secondOperand = ActUnaryOperator();

                switch (@operator)
                {
                    case Operator.Exponent:
                        firstOperand = Math.Pow(firstOperand, secondOperand);
                        break;
                }

            }
            return firstOperand;
        }

        private double ActUnaryOperator()
        {
            Operator @operator;

            if((@operator = GetOperator()) == Operator.Plus || @operator == Operator.Minus)
            {
                ChangeToken();
            }
            double result  = ActBrackets();
            if (@operator == Operator.Minus) result = -result;

            return result;
        }

        private double ActBrackets()
        {
            Operator @operator = GetOperator();
            double result;

            if (@operator == Operator.LeftBrackets)
            {
                ChangeToken();
                result = ActPlusOrMinus();
                @operator = GetOperator();

                if (@operator != Operator.RigthBrackets)
                {
                    throw new ParserException(Errors.UnbalParents);                   
                }
                ChangeToken();
            }
            else result = ActAtom();

            return result;
        }

        private double ActAtom()
        {
            double result;
            Operator @operator = GetOperator();

            if(@operator != Operator.Number)
            {
                throw new ParserException(Errors.Syntax);
            }

            if (!Double.TryParse(_token, out result))
            {
                throw new ParserException(Errors.Syntax);
            }

            ChangeToken();
            return result;
        }

        private void ChangeToken()
        {
            _token = "";

            while(_expIndex < _expression.Length && Char.IsWhiteSpace(_expression[_expIndex]))
            {
                ++_expIndex;
            }

            if (_expIndex == _expression.Length) return;

            if (IsDelim(_expression[_expIndex]))
            {
                _token += _expression[_expIndex];
                _expIndex++;
            }

            else if (Char.IsDigit(_expression[_expIndex]))
            {
                while (!IsDelim(_expression[_expIndex]))
                {
                    _token += _expression[_expIndex];
                    _expIndex++;
                    if (_expIndex >= _expression.Length) break;
                }
            }
        }

        private bool IsDelim(char c)
        {
            return (" +-/*%^=()".IndexOf(c) != -1);
        }

        private Operator GetOperator()
        {
            int tokenNumber = 0;

            if (!string.IsNullOrEmpty(_token) && Char.IsNumber(_token[tokenNumber]))
                return Operator.Number;

            switch (_token)
            {
                case "":
                    return Operator.Null;
                case "+":
                    return Operator.Plus;
                case "-":
                    return Operator.Minus;
                case "*":
                    return Operator.Multiply;
                case "/":
                    return Operator.Division;
                case "^":
                    return Operator.Exponent;
                case "(":
                    return Operator.LeftBrackets;
                case ")":
                    return Operator.RigthBrackets;
                case "%":
                    return Operator.RemaiderDivision;               
            }
            return Operator.Null;
        }
    }
}
