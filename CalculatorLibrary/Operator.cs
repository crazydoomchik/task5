﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorLibrary
{
    enum Operator
    {
        Null,
        Plus,
        Minus,
        Multiply,
        Division,
        Exponent,
        LeftBrackets,
        RigthBrackets,
        RemaiderDivision,
        Number
    }
}
