﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

namespace CalculatorLibrary.Tests
{
    public class ParseSaveFileTests
    {
        [Test()]
        [TestCaseSource(nameof(MySourceMethod))]
        public void ParseFileTest(string path, List<string> expected)
        {
            ParseSaveFile parse = new ParseSaveFile();

            parse.ParseFile(path);

            CollectionAssert.AreEqual(expected, parse.Expression);
            
        }

        [Test()]
        [TestCase("rfsfs")]
        public void ParseFileTestException(string path)
        {
            ParseSaveFile parse = new ParseSaveFile();
            try
            {
                parse.ParseFile(path);
            }
            catch (FileNotFoundException exe)
            {
                StringAssert.Contains(exe.Message, "Unable to find the specified file.");
            }
        }

        static IEnumerable<object[]> MySourceMethod()
        {
            List<string> expected = new List<string>
            {
                {"1+2*(3+2) = 11" },
                {"1+x+4 = Syntax error" },
                {"2+15/3+4*2 = 15"},
            };
            string path = @"C:\Users\crazy\source\repos\ConsoleCalculator\ParseFile.txt";
            return new[] { new object[] { path, expected }, };
        }



    }
}