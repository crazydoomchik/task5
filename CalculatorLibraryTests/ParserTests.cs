﻿using NUnit.Framework;

namespace CalculatorLibrary.Tests
{
    public class ParserTests
    {
        [Test()]
        [TestCase("1+9", 10)]
        [TestCase("(2+2)*2", 8)]
        [TestCase("10/2*8", 40)]
        [TestCase("2*2+(10*4)", 44)]
        public void EvaluateTest(string test, double expected)
        {
            Parser parser = new Parser();

            double result;
            result = parser.Evaluate(test);

            Assert.AreEqual(expected, result);

        }


        [Test()]
        [TestCase("2*2/0", 44)]
        [TestCase("2*2gdg/0", 44)]
        public void EvaluateTestException(string test, double expected)
        {
            Parser parser = new Parser();
            try
            {
                double result;
                result = parser.Evaluate(test);

                Assert.AreEqual(expected, result);
            }
            catch (ParserException ex)
            {
                Assert.IsTrue(ex is ParserException);
            }
        }
    }
}